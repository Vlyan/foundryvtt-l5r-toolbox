/**
 * @author Vlyan
 */
class VlyL5rToolbox {
    static modulePath = "modules/vly-l5r-toolbox/";

    /**
     * Dice template, needed for same order in chat and 3D dices
     * L5R Rules Book's order (p.25)
     * o: opportunities, t: strife, s: success, e: exploding success
     */
    static dicesTemplate = {
        rings: ['', 'ot', 'o', 'st', 's', 'et'],
        skills: ['', '', 'o', 'o', 'o', 'st', 'st', 's', 's', 'so', 'et', 'e']
    };

    /**
     * Current actor
     */
    static currentActor = {};

    /**
     * Helper for dices radios
     */
    static fctRadioDice(params) {
        let defaults = {name: 'radio', min: 0, max: 6, selected: null}
        params = {...defaults, ...params}
        if (params.selected === null) {
            params.selected = params.min;
        }
        let s = "";
        let id = "";
        for (let idx = 0; idx <= params.max; idx++) {
            id = `${params.name.toLowerCase()}_${idx}`;
            s += ` <input type="radio" id="${id}" name="${params.name}" value="${idx}" ${idx === params.selected ? "checked" : ""} ${idx < params.min ? "disabled" : ""}>`;
            s += ` <label for="${id}">${idx}</label>`;
        }
        return s;
    }

    /**
     * Helper for element radios
     */
    static fctRadioElements(name) {
        let s = "";
        let id = "";
        ["air", "earth", "fire", "water", "void"].forEach((element, idx) => {
            id = `${name.toLowerCase()}_${element}`;
            s += ` <input type="radio" id="${id}" name="${name}" value="${element}" ${idx === 0 ? "checked" : ""} onclick="VlyL5rToolbox.changeElementsByToken('${element}')">`;
            s += ` <label for="${id}"><i class="i_${element} ${element}" title="${game.i18n.localize('L5R.Rings.' + element)}"></i></label>`;
        });
        return s;
    }

    static changeElementsByToken(sElmt) {
        if (VlyL5rToolbox.currentActor?.data) {
            $('#ring_dices_' + VlyL5rToolbox.currentActor.data.rings[sElmt]).prop('checked', true);
        }
    }

    /**
     * Display dialog for rolling dices
     */
    static roll(params) {
        // Get Actor: sheet, selected token, nothing
        VlyL5rToolbox.currentActor = params?.actor || canvas.tokens.controlled[0]?.actor.data || null;

        let skillData = {};
        if (VlyL5rToolbox.currentActor && typeof params?.skillId != 'undefined') {
            skillData = {id: params.skillId.trim(), dices: 0, cat: "", name: ""};
            skillData.cat = Object.keys(VlyL5rToolbox.currentActor.data.skills).find(e => !!VlyL5rToolbox.currentActor.data.skills[e][skillData.id]);
            skillData.dices = VlyL5rToolbox.currentActor.data.skills[skillData.cat][skillData.id].value ?? 0;
            skillData.name = game.i18n.localize('L5R.Skills.' + skillData.cat + '.' + skillData.id);
        }

        new Dialog({
            title: "L5R Dice Roller" + (VlyL5rToolbox.currentActor ? " - " + VlyL5rToolbox.currentActor.name : ""),
            content: `<form class="noflex" autocomplete="off">
          <div class="form-group">
            <label>${game.i18n.localize('L5R.Approaches')}:</label>
            <div class="form-fields">
              ${VlyL5rToolbox.fctRadioElements("ring_type")}
            </div>
          </div>
          <hr>
          <div class="form-group">
            <label><i class="d6"></i> ${game.i18n.localize('L5R.Rings.Title')}:</label>
            <div class="form-fields">
              ${VlyL5rToolbox.fctRadioDice({name: "ring_dices"})}
            </div>
          </div>
          <hr>
          <div class="form-group">
            <label><i class="d12"></i> ${game.i18n.localize('L5R.Skill')}:${(!!skillData?.name ? " " + skillData?.name : "")}</label>
            <div class="form-fields">
              ${VlyL5rToolbox.fctRadioDice({name: "skill_dices", selected: skillData?.dices ?? 0})}
            </div>
          </div>
        </form>
        <script>VlyL5rToolbox.changeElementsByToken('air');</script>`,
            buttons: {
                yes: {
                    icon: "<i class='fas fa-check'></i>",
                    label: "Roll !",
                    callback: (html) => {
                        const sRingType = html.find('input[name="ring_type"]:checked')[0].value || null;
                        const iNbRingDices = html.find('input[name="ring_dices"]:checked')[0].value || null;
                        const iNbSkillDices = html.find('input[name="skill_dices"]:checked')[0].value || null;

                        if (!sRingType || !iNbSkillDices || !iNbRingDices || (iNbSkillDices < 1 && iNbRingDices < 1)) {
                            return false;
                        }

                        const message = game.specialDiceRoller.l5r.rollFormula(
                            "r".repeat(iNbRingDices) + "s".repeat(iNbSkillDices)
                            , `${game.i18n.localize('L5R.Rings.' + sRingType)}` + (!!skillData?.name ? ` (${skillData?.name})` : "")
                        );

                        if (typeof game.dice3d != 'undefined') {
                            // 3d Dices then chat
                            VlyL5rToolbox.diceSoNiceHack(message, true);
                        } else {
                            // chat only
                            VlyL5rToolbox.messageToChat(message, false);
                        }
                    }
                },
                no: {
                    icon: "<i class='fas fa-times'></i>",
                    label: "Cancel"
                },
            },
            default: "no"
        }).render(true);
    }

    /**
     * Send message to chat with current actor if any
     *
     * @param message String  the chat message
     * @param rolled  Boolean boolean for flags, set true if the chat already have displayed the roll
     */
    static messageToChat(message, rolled) {
        ChatMessage.create({
            flags: {vlyL5rToolbox3dRolled: rolled},
            user: game.user._id,
            speaker: ChatMessage.getSpeaker(),
            content: message,
        }, VlyL5rToolbox.currentActor ?? {});
    }

    /**
     * Display 3D Dice and chat message
     *
     * Get result from the message, this is ulgy but special roller do not have public method to get roll data
     * If you have better implementation please ping me !
     *
     * @param message    String  generated by game.specialDiceRoller.l5r.rollFormula()
     * @param sendToChat Boolean If true create message in chat, false only trigger DiceSoNice
     */
    static diceSoNiceHack(message, sendToChat) {
        try {
            const dsnDatas = {throws: [{dice: []}]};
            const rolls = Array.from(message.matchAll(/\/l5r\/(black|white)([eots]{0,3})\.png/gmu));
            rolls.forEach(res => {
                const rollRes = VlyL5rToolbox.dicesTemplate[res[1] === 'black' ? 'rings' : 'skills'].indexOf(res[2] ?? '') + 1;
                dsnDatas.throws[0].dice.push({
                    result: rollRes,
                    resultLabel: rollRes,
                    type: 'l5r' + (res[1] === 'black' ? 'r' : 's'),
                    vectors: [],
                    options: {}
                });
            });
            game.dice3d.show(dsnDatas).then(displayed => {
                if (!sendToChat) {
                    return;
                }
                VlyL5rToolbox.messageToChat(message, displayed);
            });
        } catch (e) {
            console.warn(e);
        }
    }

    /**
     * Test method for testing 3D dices display
     *
     * @param {string}  type rings|skills
     * @param {numeric} num 1-6|1-12
     */
    static testDice(type, num) {
        if (typeof type !== 'undefined' && typeof num !== 'undefined') {
            game.dice3d.show({
                throws: [{
                    dice: [{
                        result: num,
                        resultLabel: num,
                        type: 'l5r' + (type === 'rings' ? 'r' : 's'),
                        vectors: [],
                        options: {}
                    }]
                }]
            });
            return;
        }

        new Dialog({
            title: "L5R Dice Roller - Test 3D Dices",
            content: `<div class="message-content">
            <div class="special-dice-roller">
            <div>
                <span class="flavor-text">Rings Dices</span>
                <form>`
                + VlyL5rToolbox.dicesTemplate.rings.map((e, idx) =>
                    `<input type="checkbox"
                    style="background-color: black; background-image:url('${VlyL5rToolbox.modulePath}icons/${(e === '' ? 'blank' : e)}.png')"
                    onclick="VlyL5rToolbox.testDice('rings', ${idx + 1})"
                    >`) + `
                </form>
            </div>
            <hr>
            <div>
                <span class="flavor-text">Skills Dices</span>
                <form>`
                + VlyL5rToolbox.dicesTemplate.skills.map((e, idx) =>
                    `<input type="checkbox"
                    style="background-color: white; background-image:url('${VlyL5rToolbox.modulePath}icons/${(e === '' ? 'blank' : e)}.png')"
                    onclick="VlyL5rToolbox.testDice('skills', ${idx + 1})"
                    >`) + `
                </form>
            </div>
            </div>
            </div>`,
            buttons: {
                no: {
                    icon: "<i class='fas fa-times'></i>",
                    label: "Cancel"
                },
            },
            default: "no"
        }).render(true);
    }

} //class


// diceSoNice Hook
Hooks.once('diceSoNiceReady', (dice3d) => {
    const texturePath = `${VlyL5rToolbox.modulePath}icons/`;

    dice3d.addSystem({
        id: "l5r",
        name: "Legend of the Five Rings"
    }, "force");

    // Rings
    dice3d.addDicePreset({
        type: "l5rr",
        labels: VlyL5rToolbox.dicesTemplate.rings.map(e => `${texturePath}${(e === '' ? 'blank' : e)}.png`),
        bumpMaps: VlyL5rToolbox.dicesTemplate.rings.map(e => `${texturePath}${(e === '' ? 'blank' : e)}_bm.png`),
        colorset: "black",
        system: "l5r"
    }, "d6");

    // Skills
    dice3d.addDicePreset({
        type: "l5rs",
        labels: VlyL5rToolbox.dicesTemplate.skills.map(e => `${texturePath}${(e === '' ? 'blank' : e)}.png`),
        bumpMaps: VlyL5rToolbox.dicesTemplate.skills.map(e => `${texturePath}${(e === '' ? 'blank' : e)}_bm.png`),
        colorset: "white",
        system: "l5r"
    }, "d12");
});

// "/l5r" chat command render Hook
Hooks.on('renderChatMessage', (message, html, data) => {
    if (data.message.type !== 0
        || typeof game.dice3d === 'undefined'
        || data.message.content.indexOf('<div class="special-dice-roller">') < 0
        || (typeof data.message.flags.vlyL5rToolbox3dRolled !== 'undefined' && data.message.flags.vlyL5rToolbox3dRolled)
    ) {
        return;
    }
    // 3d Dices without chat (don't display it twice)
    VlyL5rToolbox.diceSoNiceHack(data.message.content, false);
});

// L5RActorSheet Skill Hook
Hooks.on("renderActorSheet", (app, html, data) => {
    html.find('.skill-name').click(ev => {
        const li = $(ev.currentTarget).parents(".skill");
        const skillId = li.data("skill");
        VlyL5rToolbox.roll({actor: data.actor, skillId: skillId});
    });
});

# foundryvtt-l5r-toolbox
L5R Toolbox for Foundry VTT

**This module is discontinued. See the new L5R5e system: https://gitlab.com/teaml5r/l5r5e :)**


## Features
- Macro Popup for choosing Element, Ring and Skill for Special dice roller.
- Skills link on character sheet
- Fixed CSS to display a red border on selected dices for "keep" / "re-roll"
- L5R "Dice So Nice" custom dices (optional, only if DsN is installed)
- Multi language : Use the L5R system languages for dialog text


## Authors
- Vlyan (vlyan#6771)


## Requirements
- L5R System ( https://github.com/L4D15/foundry-vtt-l5r )
- Special Dice Roller module ( https://github.com/BernhardPosselt/foundryvtt-special-dice-roller )
- Optional but required for 3D dices : Dice So Nice ( https://gitlab.com/riccisi/foundryvtt-dice-so-nice )


## Installation
* In Foundry, on the "Add-ons modules" tab, clic on the "Install Module" button.
* Copy and paste the following url in the "Manifest URL":
  * https://gitlab.com/Vlyan/foundryvtt-l5r-toolbox/-/raw/master/module.json
* Launch a L5R world and activate the module.
* Get the macro:
  * Open Compendium > LR5 Dice Roller Macro > Macro
  * Import the "L5R dice roller" Macro (right clic (or open) and import)
  * Go in Macros directory 
  * Drag and drop the macro in your macro bar

![alt postinstall][postinstall]


## Usage
* Press the corresponding key or the macro to display the "L5R Dice Roller" dialog windows.
* For linked attributes (rings), please select the token BEFORE displaying the dialog.

Tips : a linked dialog display the character's name in title (ex: "L5R Dice Roller - Isawa Aki") and only "L5R Dice Roller" if not


## Screenshots
![alt features][features]


## Knows bug
* Multiple dialog opened have wrong behaviors on element value


## Changelog
* 1.2 Added BumpMap for 3d Dices
* 1.1 Added hook on renderChatMessage for displaying 3D dices on "/l5r" command and for "keep" and "re-roll" buttons action
* 1.0 Initial Release


## License
This is a module for Foundry VTT and is licensed under a Creative Commons Attribution 4.0 International License.
This work is licensed under Foundry Virtual Tabletop EULA - Limited License Agreement for module development.


[features]: ./screenshots/features.png
[postinstall]: ./screenshots/postinstall.png
